import json
import re
import unidecode
from emoji import UNICODE_EMOJI
import utils as ut

PUNCTUATION = '[ .,?!:;#/\+\-\*\n()\[\]<>"]'
EMOJI_SET = set(UNICODE_EMOJI)

chat = ut.parse_json_chat('merged_chat.json')

text = '\n'.join(map(lambda m: m['text'], chat))
words = list(filter(None, re.split(PUNCTUATION, text)))

counts = {}
word_set = set()
for word in words:
    if not any([char in EMOJI_SET for char in word]):
        word = unidecode.unidecode(word.lower())
    if word in word_set:
        counts[word] += 1
    else:
        word_set.add(word)
        counts[word] = 1

sorted_counts = sorted(counts.items(), key=lambda w: w[1], reverse=True)
with open('word-count.txt', 'w', encoding='utf-8') as file:
    for word, count in sorted_counts:
        print(f'"{word}": {count}', file=file)
