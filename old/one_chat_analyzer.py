import re

SPAM_TRES = 5

spam_check = input('Check spam? (Y/n): ') != 'n'

# Finding names
print('Finding names...')

with open('chat.txt', encoding='utf-8') as chat:
    chat_txt = chat.read()
    lines = chat_txt.strip().splitlines(False)
    foundnames = re.findall(", [0-9]+:[0-2][0-9] - [^:\n]*:", chat_txt)
    names = []
    for fn in foundnames:
        editedfn = fn[fn.find(' - '):]
        if editedfn not in names:
            names.append(editedfn)

print(f'Found {len(names)} names')

# Message counting
print('Counting...')
people = []
for name in names:
    count = 0
    media_count = 0
    spam_count = 0
    one_char_count = 0
    in_a_row = 0
    for line in lines:
        if name in line:
            count += 1
            if '<Médiá vynechané>' in line:
                media_count += 1
            else:
                in_a_row += 1
            if spam_check:
                mes = ':'.join(line.split(':')[2:]).strip()
                if len(mes) == 1 or len(mes) > 400:
                    spam_count += 1
                    one_char_count += 1
        else:
            if in_a_row >= SPAM_TRES and spam_check:
                spam_count += in_a_row-1
                spam_count -= one_char_count
            in_a_row = 0
            one_char_count = 0
    people.append((name[3:-1], count, spam_count, media_count))

# Sorting
people.sort(key=lambda e: e[1]-e[2], reverse=True)

# Table generating
table = ''
for i, person in enumerate(people):
    table += '<tr>'
    table += f'<td class="num">{i+1}.</td>'
    for item in person:
        if isinstance(item, int):
            table += '<td class="num">{:,}</td>'.format(item).replace(',', ' ')
        else:
            table += '<td>{}</td>'.format(item)
    table += '</tr>'

summary = ''
for i in range(1, 4):
    summary_td = '<td class="num"><strong>{:,}</strong></td>'.format(sum([e[i] for e in people]))
    summary += summary_td.replace(',', ' ')

# HTML generating
with open('template.html', encoding='utf-8') as template:
    result = template.read()

with open('style.css') as css:
    style_lines = css.read().strip().splitlines(True)
    style = f'<style>\n{"".join([" "*6 + s for s in style_lines])}\n    </style>'

result = result.replace('<python-style />', style)
result = result.replace('<python-table />', table)
result = result.replace('<python-sum />', summary)

with open('chat_analytics.html', 'w', encoding='utf-8') as html:
    html.write(result)

print('Results exported to "chat_analytics.html"')
