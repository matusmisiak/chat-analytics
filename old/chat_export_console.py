import re

kontrolaspamu = input('Kontrolovať spam? (Y/n): ') != 'n'
print('Zisťujem mená...')

with open('chat_export/chat.txt', encoding='utf-8') as chat:
    chat_txt = chat.read()
    lines = chat_txt.strip().split('\n')
    foundnames = re.findall(", [0-9]+:[0-2][0-9] - [^:\n]*:", chat_txt)
    names = []
    for fn in foundnames:
        editedfn = fn[fn.find(' - '):]
        if editedfn not in names:
            names.append(editedfn)

message = f'Nájdené {len(names)} mená' if len(names)<5 else f'Nájdených {len(names)} mien'
print(message)
print('Počítam...')
spam_tres = 5
pocty = []
for n in names:   
    p = 0
    posebe = 0
    media = 0
    pocetspamu = 0
    for l in lines:
        if n in l:
            p += 1
            if '<Médiá vynechané>' in l:
                media += 1
            else:
                posebe += 1
            if kontrolaspamu:
                mes = ':'.join(l.split(':')[2:]).strip()
                if len(mes) == 1 or len(mes) > 400:
                    pocetspamu += 1
        else:
            if posebe >= spam_tres and kontrolaspamu:
                pocetspamu += posebe-1
            posebe = 0
    pocty.append((n[3:-1], p, pocetspamu, media))

result = '{:2s}. {:20s}|{:5s}|{:5s}|{:5s}\n'.format(' #', 'meno', 'správ', 'spamu', 'médií')
result += '-'*42 + '\n'
pocty.sort(key=lambda e: e[1]-e[2], reverse=True)
for i, m in enumerate(pocty):
    result += '{:2d}. {:20s}|{:5d}|{:5d}|{:5d}\n'.format(i+1, *m)
result += '-'*42 + '\n'
vysledky = [sum([e[1] for e in pocty]), sum([e[2] for e in pocty]), sum([e[3] for e in pocty])]
result += '{:24s}|{:5d}|{:5d}|{:5d}\n'.format('Spolu', *vysledky)

with open('chat_export/result.txt', 'w') as subor:
   print(result)
   print(result, file=subor)

print('Výsledky exportované do "results.txt"')
