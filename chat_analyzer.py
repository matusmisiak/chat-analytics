import datetime as dt
from emoji import UNICODE_EMOJI
from frozendict import frozendict
import math
import json
import os, sys
import shutil
import utils as ut

SPAM_CHECK_ARGS = ['-i', '--ignore-spam']
SPAM_TRES_ARGS = ['-s', '--spam-tres']
MERGE_ARGS = ['-m', '--do-not-merge']

DELETED_M = ['Táto správa bola odstránená', 'Túto správu ste odstránili', 'This message was deleted', 'You deleted this message']
MEDIA_M = ['<Médiá vynechané>', '<Media omitted>']

EMOJI_SET = set(UNICODE_EMOJI['en'].keys())
MAX_EMOJI_SIZE = max({len(e) for e in EMOJI_SET})

SPAM_TRES = 10
EMOJI_TRES = 15

def merge_chats(json_path1, json_path2, result_path):
    chat1 = ut.parse_json_chat(json_path1)
    chat2 = ut.parse_json_chat(json_path2)
    control_set = set([frozendict(i) for i in chat1])
    for message in chat2:
        cases = []
        for i in [-1, 0, 1]:
            f = frozendict({
                'time': message['time']+dt.timedelta(minutes=i),
                'author': message['author'],
                'text': message['text']
            })
            cases.append(f not in control_set)
        if all(cases):
            chat1.append(message)
    chat1.sort(key=lambda mes: mes['time'])
    ut.dump_json_chat(chat1, result_path)

def generate_table(people_list):
    table = ''
    keys_sum = ['messages', 'spam', 'media', 'links', 'emoji', 'chars_per_mes', 'deleted']
    keys = keys_sum.copy()
    keys.insert(0, 'author')
    keys.append('used_emoji')
    for i, person in enumerate(people_list):
        table += '<tr>'
        table += f'<td class="num">{i+1}.</td>'
        for key in keys:
            if isinstance(person[key], int) or isinstance(person[key], float):
                table += '<td class="num">{:,}</td>'.format(person[key]).replace(',', ' ')
            else:
                table += '<td>{}</td>'.format(person[key])
        table += '</tr>'
    summary = ''
    for key in keys_sum:
        val = sum([p[key] for p in people_list])
        if key == 'chars_per_mes':
            val = round(sum([p['chars'] for p in people_list])/sum([p['messages'] for p in people_list]), 2)
        summary_td = '<td class="num"><strong>{:,}</strong></td>'.format(val)
        summary += summary_td.replace(',', ' ')
    return table, summary

# Parsing arguments
args = sys.argv[1:]
if not args or not os.path.isfile(args[0]):
    raise FileNotFoundError('You have to specify a path to your chat')

filepath = args[0]
filehead, filetail = os.path.split(filepath)
filename, fileext = os.path.splitext(filetail)

SPAM_CHECK = not any([arg in args for arg in SPAM_CHECK_ARGS])
MERGE = not any([arg in args for arg in MERGE_ARGS])
spam_tres_arg = 0
for control_arg in SPAM_TRES_ARGS:
    if control_arg in args:
        try:
            SPAM_TRES = int(args[args.index(control_arg)+1])
        except (IndexError, ValueError):
            raise AttributeError('Invalid spam treshold value')

# Making directories
if not os.path.isdir('parsed_chats'):
    os.mkdir('parsed_chats')
if not os.path.isdir('results'):
    os.mkdir('results')

# Parsing and merging chats
print('Parsing chat...')
ut.parse_chat(filepath, f'parsed_chats/{filename}.json')
if MERGE:
    if not os.path.isfile('merged_chat.json'):
        shutil.copyfile(f'parsed_chats/{filename}.json', 'merged_chat.json')
    print('Merging chats...')
    merge_chats('merged_chat.json', f'parsed_chats/{filename}.json', 'merged_chat.json')
    chat = ut.parse_json_chat('merged_chat.json')
else:
    chat = ut.parse_json_chat(f'parsed_chats/{filename}.json')
people = {}
actual_time = dt.datetime(1970, 1, 1)

next_round = math.ceil(len(chat)/10000)*10000
print(f'{len(chat):,} messages parsed. ({next_round-len(chat):,} messages left to {next_round:,})')

# Counting
print('Analyzing messages...')
for message in chat:
    author = message['author']
    if author not in people.keys():
        people[author] = {
            'messages': 0,
            'spam': 0,
            'media': 0,
            'links': 0,
            'emoji':0,
            'used_emoji': {},
            'deleted': 0,
            'chars': 0,
            'mes_in_minute': 0
        }
    people[author]['messages'] += 1
    people[author]['chars'] += len(message['text'])
    emojis = 0
    if message['text'] in DELETED_M:
        people[author]['deleted'] += 1
    elif message['text'] not in MEDIA_M:
        text = message['text']
        for step in range(MAX_EMOJI_SIZE, 0, -1):
            for i in range(0, len(text), step):
                substring = text[i:(i+step)]
                if substring in EMOJI_SET:
                    emojis += 1
                    if substring not in people[author]['used_emoji']:
                        people[author]['used_emoji'][substring] = 0
                    people[author]['used_emoji'][substring] += 1
                    text = text[:i]+text[i+step:]
        people[author]['emoji'] += emojis
    if SPAM_CHECK and emojis >= EMOJI_TRES:
        people[author]['spam'] += 1
    if message['text'] in MEDIA_M:
        people[author]['media'] += 1
    if any([s in message['text'] for s in ['https://', 'http://', 'www.']]):
        people[author]['links'] += 1
    elif SPAM_CHECK:
        if message['time'] == actual_time:
            people[author]['mes_in_minute'] += 1
        else:
            actual_time = message['time']
            for auth in people:
                if people[auth]['mes_in_minute'] >= SPAM_TRES:
                    people[auth]['spam'] += people[auth]['mes_in_minute']
                people[auth]['mes_in_minute'] = 0

for author, value in people.items():
    e = value['used_emoji'].items()
    sorted_emoji = [(k, v) for k, v in sorted(e, key=lambda i: i[1], reverse=True)]
    emoji_strings = ['{}({})'.format(*e) for e in sorted_emoji[:5]]
    people[author]['used_emoji'] = ' '.join(emoji_strings)
    people[author]['chars_per_mes'] = round(people[author]['chars']/people[author]['messages'], 2)
    people[author]['author'] = author

people_list = list(people.values())

people_list.sort(key=lambda p: p['messages']-p['spam'], reverse=True)

print('Generating results...')
table, summary = generate_table(people_list)

# HTML generating
with open('template.html', encoding='utf-8') as template:
    result = template.read()

with open('style.css') as css:
    style_lines = css.read().strip().splitlines(True)
    style = f'<style>\n{"".join([" "*6 + s for s in style_lines])}\n    </style>'

result = result.replace('<python-style />', style)
result = result.replace('<python-table />', table)
result = result.replace('<python-sum />', summary)

name = f'{filename}_{chat[-1]["time"].strftime(ut.DATE_FORMAT)}'
res_path = f'results/{name}_analysis.html'
with open(res_path, 'w', encoding='utf-8') as html:
    html.write(result)

print(f'Results exported to "{res_path}"')

# Graph generating
try:
    import chat_graph as graph
    print('Generating graph...')
    graph.make_graph(chat, name)
except ModuleNotFoundError:
    print("Couldn't generate graph, matplotlib not found")
