import os
import json
import datetime as dt
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import utils as ut

def make_graph(chat, name=None, days=7):
    dates = list(map(lambda m: m['time'].date(), chat))

    bins = [dates[0]+d*dt.timedelta(days=1) for d in range(0, (dates[-1]-dates[0]).days+1, days)]

    if not name:
        name = dates[-1].strftime(f'merged_chat_{ut.DATE_FORMAT}')
    fig, ax = plt.subplots()
    fig.set_size_inches(10, 5)
    fig.autofmt_xdate()
    plt.title(f'{dates[0].strftime(ut.DATE_FORMAT)} to {dates[-1].strftime(ut.DATE_FORMAT)}')
    ax.xaxis.set_minor_locator(mdates.MonthLocator())
    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
    ax.grid(which='major')
    ax.grid(which='minor', color='lightgrey')
    ax.hist(dates, bins=bins)

    if not os.path.isdir('graphs'):
        os.mkdir('graphs')

    plt.savefig(f'graphs/{name}.png')

if __name__ == '__main__':
    make_graph(ut.parse_json_chat('merged_chat.json'))
