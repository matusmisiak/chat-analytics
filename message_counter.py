import datetime

meno = input('Meno alebo ID: ')
datum = input('Dátum: ')

with open('chat_export/chat.txt', encoding='utf-8') as chat:
    lines = chat.read().strip().split('\n')
    
with open('chat_export/zoznamTRIEDA.txt', encoding='utf-8') as subor:
    names = subor.read().strip().split('\n')

if meno.isnumeric():
    meno = names[int(meno)-1]
if datum == 'dnes':
    datum = '{dt.day}. {dt.month}. {dt.year}'.format(dt=datetime.datetime.now())
    
print('Hľadám správy pre meno "{}"{}{}'.format(meno, ' z dňa ' if datum else '', datum))
p = 0
for l in lines:
    if ' - {}:'.format(meno) in l and datum in l:
        p += 1
    
print('{} správ'.format(p))