import re
import json
import datetime as dt

CHAT_TIME_FORMAT = '%d/%m/%Y, %H:%M - '
TIME_FORMAT = '%Y-%m-%dT%X'
DATE_FORMAT = '%Y-%m-%d'

def parse_chat(chat_path, json_path):
    with open(chat_path, encoding='utf-8-sig') as chat:
        chat_txt = chat.read()
    lines = chat_txt.strip().splitlines(False)
    messages = []
    for l in lines:
        time = re.search('^[0-9]+/[0-9]+/[0-9]+, [0-9]+:[0-9]+ - ', l)
        author = re.search(' - [^:]*:', l)
        text = re.search(': .*', l)
        if time:
            message = {
                'time'   : dt.datetime.strptime(time.group(), CHAT_TIME_FORMAT),
                'author' : author.group()[3:-1] if author else 'WhatsApp',
                'text'   : text.group()[2:].strip() if text else ''
            }
            messages.append(message)
        else:
            messages[-1]['text'] = messages[-1]['text']+'\n'+l.strip()
    dump_json_chat(messages, json_path)

def dump_json_chat(messages, json_path):
    for m in messages:
        m['time'] = m['time'].strftime(TIME_FORMAT)
    with open(json_path, 'w', encoding='utf-8') as parsed_chat:
        parsed_chat.write(json.dumps(messages, ensure_ascii=False, indent=2))

def parse_json_chat(json_path):
    with open(json_path, encoding='utf-8') as chat:
        messages = json.load(chat)
    for m in messages:
        m['time'] = dt.datetime.strptime(m['time'], TIME_FORMAT)
    return messages
